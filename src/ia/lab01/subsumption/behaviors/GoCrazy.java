package ia.lab01.subsumption.behaviors;

import java.util.Random;

import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Behavior;

public class GoCrazy implements Behavior{
	//TODO: Implement LOL
	DifferentialPilot woboPilot;
	final int maxTravelDistance = 50, maxRotationAngle = 180;
	public GoCrazy(DifferentialPilot woboPilot){
		this.woboPilot = woboPilot;
	}
	@Override
	public boolean takeControl() {
		return false;
	}

	@Override
	public void action() {
		Random r = new Random();
		//Choose a random movement
		this.woboPilot.rotate((r.nextDouble() * maxRotationAngle));
		this.woboPilot.travel((r.nextDouble() * this.maxTravelDistance), true);
	}

	@Override
	public void suppress() {
		
	}

}

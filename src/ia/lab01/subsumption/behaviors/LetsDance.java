package ia.lab01.subsumption.behaviors;

import ia.lab01.subsumption.Jukebox;

import lejos.robotics.subsumption.Behavior;

public class LetsDance implements Behavior{
	//TODO: Implement LOL
	Jukebox woboJukebox;
	@Override
	public boolean takeControl() {
		//TODO: return true if n claps
		return true;
	}

	@Override
	public void action() {
		this.woboJukebox = new Jukebox();
		this.woboJukebox.play(Jukebox.STARWARS_INTRO, false);
		//TODO: Move in a dancing pattern
	}

	@Override
	public void suppress() {
		this.woboJukebox.poweroff();		
	}

}

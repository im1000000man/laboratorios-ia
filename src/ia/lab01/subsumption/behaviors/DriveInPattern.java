package ia.lab01.subsumption.behaviors;

import ia.lab01.subsumption.MainWobot;

import lejos.nxt.*;
import lejos.robotics.subsumption.*;

public class DriveInPattern  implements Behavior {
   private boolean suppressed = false;
   private LightSensor light;
   private int lightThreshold = 60;
   
   public DriveInPattern(SensorPort port){
	   light = new LightSensor(port);
   }
   
   public boolean takeControl() {
	   //TODO: Test Light threshold value 
      return (light.getLightValue() > lightThreshold);
   }

   public void suppress() {
      suppressed = true;
   }

   public void action() {
	 //TODO: Change to a more interesting driving pattern
     suppressed = false;
    MainWobot.leftMotor.forward();
    MainWobot.rightMotor.forward();
     while( !suppressed )
        Thread.yield();
     MainWobot.leftMotor.stop(); // clean up
     MainWobot.rightMotor.stop();
   }
}

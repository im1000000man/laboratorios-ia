package ia.lab01.subsumption.behaviors;

import lejos.nxt.*;
import lejos.robotics.subsumption.*;

public class Avoid implements Behavior {
    private TouchSensor touchLeft;
    private TouchSensor touchRight;
    private UltrasonicSensor sonar;
    private boolean suppressed = false;
    private final double proximityThreshold = 15.0;
    public Avoid(SensorPort ultrasonicPort, SensorPort touchPort1, SensorPort touchPort2 )
    {
       sonar = new UltrasonicSensor( ultrasonicPort );
       touchLeft = new TouchSensor( touchPort1);
       touchRight = new TouchSensor( touchPort2);
    }

    public boolean takeControl() {
    	return touchLeft.isPressed() || touchRight.isPressed() || sonar.getDistance() < proximityThreshold;
    }

    public void suppress() {
       suppressed = true;
    }

    public void action() {
       suppressed = false;
       Motor.A.rotate(-180, true);
       Motor.C.rotate(-360, true);

       while( Motor.C.isMoving() && !suppressed )
         Thread.yield();

       Motor.A.stop();
       Motor.C.stop();
    }
}
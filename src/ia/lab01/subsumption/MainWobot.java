package ia.lab01.subsumption;

import ia.lab01.subsumption.behaviors.Avoid;
import ia.lab01.subsumption.behaviors.DriveInPattern;
import ia.lab01.subsumption.behaviors.GoCrazy;
import ia.lab01.subsumption.behaviors.LetsDance;

import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class MainWobot {
	public static final NXTRegulatedMotor leftMotor = Motor.B, rightMotor = Motor.C;
	
	public static void main(String [] args) {
	    //TODO: Verify that each sensor port matches between the code and the physical connection port number.  
		//TODO: Measure wheel diameter, trackWidth
		DifferentialPilot woboPilot = new DifferentialPilot(8.0, 16.0, leftMotor , rightMotor);
		Behavior fixedPattern = new DriveInPattern(SensorPort.S4);
	    Behavior dance = new LetsDance();     
		Behavior random = new GoCrazy(woboPilot);
	    Behavior avoid = new Avoid(SensorPort.S1, SensorPort.S2, SensorPort.S3);	    
	    Behavior [] bArray = {fixedPattern, dance, random,  avoid};
	    Arbitrator arbitrator = new Arbitrator(bArray);
	    arbitrator.start();
	}
}



package ia.lab01;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

import lejos.geom.Point;
import lejos.nxt.Motor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Move;
import lejos.robotics.navigation.MoveListener;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.util.Delay;

public final class RobotNXT implements Runnable {

	public static interface RobotNXTState {
		public void onEnter(final String prev, final RobotNXT r, final Object ... args);
		public void onAction(final RobotNXT r);
		public void onLeave(final RobotNXT r);
	}

	private final static RobotNXTState IDLE_STATE = new RobotNXTState() {
		@Override
		public void onEnter(final String prev, final RobotNXT r, final Object ... args) { ; }
		@Override
		public void onAction(final RobotNXT r) { ; }
		@Override
		public void onLeave(final RobotNXT r) { ; }
	};
	private final static RobotNXTState END_STATE = new RobotNXTState() {
		@Override
		public void onEnter(final String prev, final RobotNXT r, final Object ... args) {
			
			System.out.println("Entering state: __end__");
			r.stop();
		}
		@Override
		public void onAction(final RobotNXT r) { ; }
		@Override
		public void onLeave(final RobotNXT r) { ; }
	};

	private final Hashtable<String, FeatureDetector> features;
	private final Hashtable<String, RobotNXTState> states;
	private final DifferentialPilot navigator;
	private final Stack<String> state_stack;
	private RobotNXTState current_state;
	
	public RobotNXT(final double speed) { this(8.0, 16.0, speed); }
	public RobotNXT(final double diameter, final double wheel_span, final double speed) {
		/* Assumes the robot is the standard "Wall-e" configuration with identical wheels and
		 *  single axis. */
		
		// Initialize inner state
		this.navigator = new DifferentialPilot(diameter, wheel_span, Motor.B, Motor.C);
		this.features = new Hashtable<String, FeatureDetector>();
		this.states = new Hashtable<String, RobotNXTState>();
		this.state_stack = new Stack<String>();
		this.current_state = RobotNXT.IDLE_STATE;

		// Setting up navigator
		this.navigator.setTravelSpeed(speed);
		
		// Initializing and setting up state machine
		this.registerState("__end__", RobotNXT.END_STATE);
		this.save("__end__");
	}

	public String registerFeature(final String name, final FeatureDetector feature) {

		this.features.put(name, feature);
		return name;
	}
	public <F extends FeatureDetector> F requestFeature(final String name, final Class<F> feature_type) {
		
		if (this.features.get(name) == null) 
			throw new IllegalStateException(new StringBuilder("Feature ").append(name).append(" is unavailable or was not registered.").toString());
		
		return feature_type.cast(this.features.get(name));
	}
	public String registerState(final String name, final RobotNXTState state) {
		
		this.states.put(name, state);
		return name;
	}
	public void hintState(final String name, final Object ... args) {
		
		final String current = this.getCurrentState();
		if (!name.equals(current)) {
			
			if (!this.hasState(name))
				throw new IllegalStateException("State " + name + " is not defined");

			final RobotNXTState next = this.states.get(name);
			
			// Perform transitions and change states
			this.current_state.onLeave(this);
			next.onEnter(current, this, args);
			
			this.current_state = next;
		}
	}
	public void save(final String name) {
		
		if (!this.current_state.equals(name))
			this.state_stack.push(name);
	}
	public String restore() {
		
		if (this.state_stack.size() > 0)
			return this.state_stack.pop();
		
		return "__end__";
	}
	public void registerMovementListener(final MoveListener l) {
		this.navigator.addMoveListener(l);
	}
	public void moveTowards(final Point p) {

		this.moveTowards(p, new Runnable() {
			@Override
			public void run() { ; }
		});
	}
	public void moveTowards(final Point p, final Runnable onMove) {

		final Point polar = this.toPolar(p);

		this.navigator.rotate(polar.y);
		this.navigator.travel(polar.x, true);
		
		while (this.navigator.isMoving())
			onMove.run();
	}
	public void rotate(final double angle) { this.navigator.rotate(angle); }
	public void delay(final long ms) { Delay.msDelay(ms); }
	public void stop() { this.stop(false); }
	public void stop(final boolean immediate) {
		
		if (immediate)
			this.navigator.quickStop();
		else
			this.navigator.stop();
	}
	public Point estimateCurrentPosition() {

		final Move m = this.navigator.getMovement();
		return this.toCartesian(m.getDistanceTraveled(), m.getAngleTurned());
	}
	public Point toPolar(final Point p) {

		final double radius = Math.sqrt((p.x * p.x) + (p.y * p.y)),
				angle = Math.atan2(p.y, p.x) * 57.2957795131;
		
		return new Point((float) (radius), (float) (angle));
	}
	public Point toCartesian(double radius, double angle) {

		angle *= 0.01745329251;
		radius = (1.0 / radius);

		final double x = (Math.cos(angle) * radius),
				y = (Math.sin(angle) * radius);
		
		return new Point((float) (x), (float) (y));
	}
	public String getCurrentState() {

		if (this.state_stack.size() > 0)
			return this.state_stack.peek();

		return "__end__";
	}
	private boolean hasState(final String name) {
		
		final Enumeration<String> keys = this.states.keys();
		while (keys.hasMoreElements()) {
			
			if (keys.nextElement().equals(name))
				return true;
		}
		
		return false;
	}
	
	@Override
	public void run() {

		while (true) {

			this.current_state.onAction(this);

			if (this.current_state == RobotNXT.END_STATE)
				break;
		}

		this.current_state.onLeave(this);
	}
	
}

package ia.lab01;

import ia.lab01.RobotNXT.RobotNXTState;

import lejos.geom.Point;
import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.RangeFeatureDetector;
import lejos.robotics.objectdetection.TouchFeatureDetector;

public final class Main {

	private final static RobotNXTState INITIAL = new RobotNXTState() {

		@Override
		public void onLeave(final RobotNXT r) { ; }
		@Override
		public void onEnter(final String prev, final RobotNXT r, final Object ... args) {
			
			System.err.println("Entering state: \n initial");
			
			r.registerFeature("range", new RangeFeatureDetector(new UltrasonicSensor(SensorPort.S1), 150, 50));
			r.registerFeature("touch-left", new TouchFeatureDetector(new TouchSensor(SensorPort.S2), -20, -5));
			r.registerFeature("touch-right", new TouchFeatureDetector(new TouchSensor(SensorPort.S3), 20, -5));
		}
		@Override
		public void onAction(final RobotNXT r) {
			
			Button.waitForAnyPress();
			r.hintState("wander");
		}

	};
	private final static RobotNXTState WANDERING = new RobotNXTState() {

		private final static double
			MAX_WANDER_DISTANCE = 100.0,
			PROXIMITY_TRESHOLD = 15.0
		;
		private RangeFeatureDetector range;
		
		@Override
		public void onLeave(final RobotNXT r) { ; }
		@Override
		public void onEnter(final String prev, final RobotNXT r, final Object ... args) {
			
			System.err.println("Enter state: wandering");
			
			this.range = r.requestFeature("range", RangeFeatureDetector.class);
		}
		@Override
		public void onAction(final RobotNXT r) {

			r.moveTowards(this.getRandomDirection(MAX_WANDER_DISTANCE, r), new Runnable() {

				@Override
				public void run() {
					
					final Feature readings = range.scan();
					if (readings != null) {
						
						final double dist = readings.getRangeReading().getRange();
						if (dist < PROXIMITY_TRESHOLD) {
							
							r.stop();
							r.save("wander");
							r.hintState("avoid", dist, r.estimateCurrentPosition());
							
							return;
						}
					}
				}

			});
		}
		
		private Point getRandomDirection(final double max_distance, final RobotNXT r) {

			final double angle = (Math.atan2(Math.random(), Math.random()) * 57.2957795131), distance = (1.0 / (Math.random() * max_distance));
			return r.toCartesian(distance, angle);
		}
		
	};
	private final static RobotNXTState AVOIDING_OBSTACLE = new RobotNXTState() {

		private final static double
			SIDE_DISTANCE = 100.0,
			PROXIMITY_TRESHOLD = 15.0
		;

		private TouchFeatureDetector left, right;
		private double last_reading;
		private Point start;
		
		@Override
		public void onLeave(final RobotNXT r) { ; }
		@Override
		public void onEnter(final String prev, final RobotNXT r, final Object ... args) {
			
			System.err.println("Enter state: avoid");
			
			this.left = r.requestFeature("touch-left", TouchFeatureDetector.class);
			this.right = r.requestFeature("touch-right", TouchFeatureDetector.class);
			
			this.last_reading = ((double) (args[0]));
			this.start = ((Point) (args[1]));
		}
		@Override
		public void onAction(final RobotNXT r) {

			r.moveTowards(this.getSteeringDirectionFrom(this.start, this.last_reading, r), new Runnable() {

				@Override
				public void run() { ; }

			});
			
			r.hintState(r.restore());
		}

		private Point getSteeringDirectionFrom(final Point start, final double last_reading, final RobotNXT r) {
			
			final double lread = Math.max(last_reading, this.getReadingFor(this.left)), 
						 rread = Math.max(last_reading, this.getReadingFor(this.right));
			
			if (lread > rread) return r.toCartesian(10.0, -45.0);
			else if (rread > lread) return r.toCartesian(10.0, 45.0);
			else return r.toCartesian(20.0, 90.0);
		}
		private double getReadingFor(final TouchFeatureDetector touch){
			
			final Feature f = touch.scan();
			
			if (f != null)
				return f.getRangeReading().getRange();
			
			return -1.0;
		}
		
	};
	
	public static void main(final String[] args) {
		
		final RobotNXT r = new RobotNXT(60.0);

		r.registerState("initial", Main.INITIAL);
		r.registerState("wander", Main.WANDERING);
		r.registerState("avoid", Main.AVOIDING_OBSTACLE);
		r.hintState("initial", new Object[0]);

		new Thread(r).start();
	}

}

package ia.lab02;

import lejos.geom.Point;
import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.SoundSensor;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import ia.lab02.NXTRobot.NXTBehavior;

public final class Main {

	public final static NXTBehavior WANDER = new NXTBehavior() {

		private boolean suppressed = false;

		@Override
		public boolean takeControl(final NXTRobot r) { return true; }
		@Override
		public void action(final NXTRobot r) {
			
			System.out.println("State: wander");
			
			this.suppressed = false;
			final Point p = this.getRandomDirection(60.0);
			r.moveTowards(p.x, p.y);

			while (!this.suppressed && r.isMoving())
				Thread.yield();
		}
		@Override
		public void suppress(final NXTRobot r) { this.suppressed = true; }

		private final Point getRandomDirection(final double max_distance) {

			final double angle = (Math.atan2(Math.random(), Math.random()) * 57.2957795131),
						 distance = (Math.random() * max_distance);

			return new Point(((float) (distance)), ((float) (angle)));
		}

	};
	public final static NXTBehavior PATTERN_MOVE = new NXTBehavior() {

		public final static int SOUND_PERCENTAGE_THRESHOLD = 40;

		private boolean suppressed = false;

		@Override
		public boolean takeControl(final NXTRobot r) {
			
			final SoundSensor sound = r.requestSensor("sound", SoundSensor.class);
			final TouchSensor left = r.requestSensor("bumper-left", TouchSensor.class),
					  		  right = r.requestSensor("bumper-right", TouchSensor.class);
			
			return ((left.isPressed() || right.isPressed()) && (sound.readValue() > SOUND_PERCENTAGE_THRESHOLD));
		}
		@Override
		public void action(final NXTRobot r) {
			
			System.out.println("State: pattern move");
			
			for (int i = 0; i < 6; i++) {
				
				r.rotate(90.0);
				r.advance(40.0);
				
				while (!this.suppressed && r.isMoving())
					Thread.yield();
			}
		}
		@Override
		public void suppress(final NXTRobot r) { this.suppressed = true; }

	};
	public final static NXTBehavior DANCE = new NXTBehavior() {
		Jukebox jukebox;
		
		public final static int SOUND_PERCENTAGE_THRESHOLD = 92;

		private boolean suppressed = false;
		private double direction = 1.0;

		@Override
		public boolean takeControl(final NXTRobot r) {
			
			final SoundSensor sound = r.requestSensor("sound", SoundSensor.class);
			return (sound.readValue() > SOUND_PERCENTAGE_THRESHOLD);
		}
		@Override
		public void action(final NXTRobot r) {

			System.out.println("State: dance");
			this.suppressed = false;
			
			// Let's party like it's 1985
			r.advance(20.0);
			// Let's play the Starwars Song		
			jukebox = new Jukebox();
			jukebox.play(Jukebox.STARWARS_INTRO, false);
			r.rotate(360.0 * this.direction);
			this.direction *= -1.0;
			r.advance(40.0);
			r.rotate(360.0 * this.direction);			
			r.advance(20.0);

			while (!this.suppressed && r.isMoving())
				Thread.yield();
		}
		@Override
		public void suppress(final NXTRobot r) { 
			jukebox.poweroff();
			this.suppressed = true; }

	};
	public final static NXTBehavior EVADE = new NXTBehavior() {

		public final static double MAX_DISTANCE_THRESHOLD = 25.0;

		private boolean suppressed = false;

		@Override
		public boolean takeControl(final NXTRobot r) {
			
			final UltrasonicSensor range = r.requestSensor("range", UltrasonicSensor.class);
			final TouchSensor left = r.requestSensor("bumper-left", TouchSensor.class),
							  right = r.requestSensor("bumper-right", TouchSensor.class);

			return (left.isPressed() || right.isPressed() || (range.getRange() <= MAX_DISTANCE_THRESHOLD));
		}
		@Override
		public void action(final NXTRobot r) {
			
			System.out.println("State: evade");
			this.suppressed = false;
			
			r.stop();
			r.advance(-20);
			r.moveTowards(60.0, 180.0);
			while (!this.suppressed && r.isMoving())
				Thread.yield();
		}
		@Override
		public void suppress(final NXTRobot r) { this.suppressed = true; }

	};
	public final static NXTBehavior BYEBYE = new NXTBehavior() {

		private boolean suppressed = false;

		@Override
		public boolean takeControl(final NXTRobot r) {	
			return  Button.ESCAPE.isDown();
		}
		@Override
		public void action(final NXTRobot r) {		
			System.exit(0);
		}
		@Override
		public void suppress(final NXTRobot r) { 			
			this.suppressed = true; 
		}

	};
	public final static NXTBehavior FREEZE = new NXTBehavior() {

		private boolean suppressed = false;

		@Override
		public boolean takeControl(final NXTRobot r) {	
			return  Button.ENTER.isDown();
		}
		@Override
		public void action(final NXTRobot r) {	
			System.out.println("¡FREEZE!");
			r.stop();			
		}
		@Override
		public void suppress(final NXTRobot r) { this.suppressed = true; }

	};
	public static void main(final String ... args) {

		final NXTRobot r = new NXTRobot(20.0);
		// Register features
		r.registerSensor("range", new UltrasonicSensor(SensorPort.S1));
		r.registerSensor("bumper-left", new TouchSensor(SensorPort.S2));
		r.registerSensor("bumper-right", new TouchSensor(SensorPort.S3));
		r.registerSensor("sound", new SoundSensor(SensorPort.S4));

		// Register states
		r.registerBehavior(Main.WANDER);
		r.registerBehavior(Main.DANCE);
		r.registerBehavior(Main.EVADE);
		r.registerBehavior(Main.PATTERN_MOVE);
		r.registerBehavior(Main.BYEBYE);
		r.registerBehavior(Main.FREEZE);

		r.run();
	}

}

package ia.lab02;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import lejos.geom.Point;
import lejos.nxt.Motor;
import lejos.nxt.SensorConstants;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
import lejos.util.Delay;

public final class NXTRobot implements Runnable {

	public static interface NXTBehavior {

		public boolean takeControl(final NXTRobot r);
		public void action(final NXTRobot r);
		public void suppress(final NXTRobot r);

	}

	private final Hashtable<String, SensorConstants> features;
	private final DifferentialPilot navigator;
	private final List<Behavior> behaviors;

	public NXTRobot(final double speed) { this(8.0, 16.0, speed); }
	public NXTRobot(final double diameter, final double wheel_span, final double speed) {

		this.behaviors = new LinkedList<Behavior>();
		this.navigator = new DifferentialPilot(diameter, wheel_span, Motor.B, Motor.C);
		this.features = new Hashtable<String, SensorConstants>();
	}

	public String registerSensor(final String name, final SensorConstants feature) {

		this.features.put(name, feature);
		return name;
	}
	public <S extends SensorConstants> S requestSensor(final String name, final Class<S> feature_type) {

		if (this.features.get(name) == null)
			throw new IllegalStateException(new StringBuilder("Feature ").append(name).append(" is unavailable or was not registered.").toString());

		return feature_type.cast(this.features.get(name));
	}
	public void registerBehavior(final NXTBehavior b) {

		this.behaviors.add(new Behavior() {

			@Override
			public boolean takeControl() { return b.takeControl(NXTRobot.this); }
			@Override
			public void action() { b.action(NXTRobot.this); }
			@Override
			public void suppress() { b.suppress(NXTRobot.this); }

		});
	}

	public boolean isMoving() { return this.navigator.isMoving(); }
	public void advance(final double distance) { this.navigator.travel(distance); }
	public void rotate(final double angle) { this.navigator.rotate(angle); }
	public void moveTowards(final double distance, final double angle) {

		this.navigator.rotate(angle);
		this.navigator.travel(distance, true);
	}
	public void delay(final long ms) { Delay.msDelay(ms); }
	public void stop() { this.stop(false); }
	public void stop(final boolean immediate) {

		if (immediate) this.navigator.quickStop();
		else this.navigator.stop();
	}
	public void accelerate(final double speed) {

		this.navigator.setTravelSpeed(speed);
		this.navigator.setRotateSpeed(speed);
	}

	@Override
	public void run() {
		new Arbitrator(this.behaviors.toArray(new Behavior[this.behaviors.size()])).start();
	}

}

package ia.lab02;

import lejos.geom.Point;

public final class Polar {

	public static Point toPolar(final double x, final double y) {

		final double radius = Math.sqrt((x * x) + (y * y)),
					 angle = Math.atan2(y, x) * 57.2957795131;

		return new Point((float) (radius), (float) (angle));
	}
	public static Point toPolar(final Point p) { return Polar.toPolar(p.x,  p.y); }
	public static Point fromPolar(double angle, double radius) {

		angle *= 0.01745329251;
		radius = (1.0 / radius);

		final double x = (Math.cos(angle) * radius),
					 y = (Math.sin(angle) * radius);

		return new Point((float) (x), (float) (y));
	}
	public static Point fromPolar(final Point p) { return Polar.fromPolar(p.x, p.y); }

}
